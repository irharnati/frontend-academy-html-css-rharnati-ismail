module.exports = function(grunt) {

  grunt.initConfig({
    jshint: {
      files: ['Gruntfile.js', 'src/**/*.js', 'test/**/*.js'],
      options: {
        globals: {
          jQuery: true
        }
      }
    },
    watch: {
      files: 'src/*.scss',

      tasks: ['sass']
    },

     cssmin: {
      target: {
        files: [{
          expand: true,
          src: ['style/*.css', '!style/*.min.css'],
          dest: '',
          ext: '.min.css'
        }]
      }
    },

    sass: {                              // Task
      dist: {                            // Target
        options: {                       // Target options
          style: 'expanded'
        },
        files: {                         // Dictionary of files
          'style/style.css': 'src/style.scss'
        }
     }
  }
});
  

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  
  grunt.registerTask('default', ['cssmin']);
  grunt.registerTask('default', ['jshint']);
  grunt.registerTask('default', ['sass']);
  grunt.registerTask('default', ['watch']);
  ;


};